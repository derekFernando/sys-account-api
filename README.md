# About the API
This API will take the following data model and will commit it to a database. It supports the full CRUD operations but also full and incremental updates. 

## Link to BitBucket
https://bitbucket.org/derekFernando/sys-account-api/src/master/

## Tools Used to Develop and Test
Postman
MySql
Mulesoft: Anypoint Studio

## Prerequisite to execute the API

### 1. In MySQL
It must be deployed in http://localhost:3306
User must be `root`
Password must also be `root`
Create a schema named `testschema`
Then import 3 SQL Schema provided

### 2. Clone Source Code in Bitbucket


### 3. In Anypoint Studio
Import the Cloned Folder (sys-account-api) in Anypoint Studio as `Anypoint Studio Project from File System`
Right Click on the imported application then click `Run As -> 2 Mule Application`
Wait Console to show "Deployed"

### 3. Download Postman Collection
Download and Import the Collection provided


# Executing the API

## Post Account
`POST /account/`

### Request
	curl --location --request POST 'http://localhost:8081/api/account' \
	--header 'Content-Type: application/json' \
	--data-raw '{
	  "Identification": {
	    "FirstName": "James",
	    "LastName": "Jones",
	    "DOB": "1/28/1965",
	    "Gender": "M",
	    "Title": "Dr"
	  },
	  "Address": [    
	    {
	      "type": "home",
	      "number": 601,
	      "street": "Zapatera St",
	      "Unit": "303",
	      "City": "Cebu City",
	      "State": "Cebu",
	      "zipcode": "6000"
	    }
	  ],
	  "Communication": [
	    {
	      "type": "email",
	      "value": "jamesJones@gmail.com",
	      "preferred": "true"
	    },
	    {
	      "type": "Cellphone",
	      "value": "09215977799"
	    }
	  ]
	}'

### Response
    {
	  "status": "SUCCESS",
	  "message": "Successfully Created",
	  "AccountID": 12
	}

## Get Account

`Get /account/`

### Request
	curl --location --request GET 'http://localhost:8081/api/account/' \--header 'Content-Type: application/json'

### Response
Gets All Accounts 

## Get Specfic Account

`Get /account/{AccountID}`
Use AccountID from response of POST call

### Request
	curl --location --request GET 'http://localhost:8081/api/account/12' \--header 'Content-Type: application/json'

### Response
	{
	    "/12": {
	        "Identification": {
			    "FirstName": "James",
			    "LastName": "Jones",
			    "DOB": "1/28/1965",
			    "Gender": "M",
			    "Title": "Dr"
			  },
	        "Address": [    
			    {
			      "type": "home",
			      "number": 601,
			      "street": "Zapatera St",
			      "Unit": "303",
			      "City": "Cebu City",
			      "State": "Cebu",
			      "zipcode": "6000"
			    }
			  ],
			  "Communication": [
			    {
			      "type": "email",
			      "value": "jamesJones@gmail.com",
			      "preferred": "true"
			    },
			    {
			      "type": "Cellphone",
			      "value": "09215977799"
			    }
			  ]
	    }
	}


## PUT Specfic Account

`Put /account/{AccountID}`
Use AccountID from response of POST call
Payload must be of equal size as the Payload during account creation


### Request
	curl --location --request PUT 'http://localhost:8081/api/account/12' \
	--header 'Content-Type: application/json' \
	--data-raw '{
	  "Identification": {
	    "FirstName": "Derek",
	    "LastName": "Fernando",
	    "DOB": "05/21/1994",
	    "Gender": "M",
	    "Title": "Mr"
	  },
	  "Address": [
	    {
	      "type": "home",
	      "number": 21,
	      "street": "Nichols Heights",
	      "Unit": "21-H",
	      "City": "Cebu City",
	      "State": "Cebu",
	      "zipcode": "6000"
	    }
	  ],
	  "Communication": [
	    {
	      "type": "email",
	      "value": "derekanthonyfernando@gmail.com",
	      "preferred": "true"
	    },
	    {
	      "type": "Cellphone",
	      "value": "09992277426"
	    }
	  ]
	}'

### Response
	{
	    "status": "SUCCESS",
	    "message": "Updated Account!"
	}


## DELETE Specfic Account

`Delete /account/{AccountID}`
Use AccountID from response of POST call

### Request
	curl --location --request DELETE 'http://localhost:8081/api/account/12' \ --header 'Content-Type: application/json'

### Response
	{
    "status": "SUCCESS",
    "message": "Successfully Deleted"
	}


## Download MySQL
Download here : https://dev.mysql.com/downloads/installer/

## Download Postman
Download here : https://www.postman.com/downloads/

## Download Anypoint Studio
Download here : https://www.mulesoft.com/lp/dl/studio